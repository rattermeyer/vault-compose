import hvac

client = hvac.Client(url='http://localhost:8200')

is_initialized = client.sys.is_initialized()

if not is_initialized:
    shares = 5
    threshold = 3

    result = client.sys.initialize(shares, threshold)

    root_token = result['root_token']
    keys = result['keys']

    print(keys)
    print(keys)
else:
    print("initialized")


