import os
import hvac


def main():
    vault_addr = os.environ.get('VAULT_ADDR','http://localhost:8200')
    client = hvac.Client(url=vault_addr)
    keys = []

    if not client.sys.is_initialized():
        shares=5
        threshold=3
        result = client.sys.initialize(shares, threshold)
        root_token= result['root_token']
        keys = result['keys']
        print("The Keys:")
        i: int
        for i in range(len(keys)):
            print("VAULT_UNSEAL_KEY_{}={};".format(i+1, keys[i]),end='')

        print()
        print("The Root Token VAULT_ROOT_TOKEN={}".format(root_token))
        print("""
        Please add these as environment variables to the docker container. 
        Otherwise, you will not be able to able to recover you ci vault.
        """)
    if len(keys) == 0:
        print("trying to get unseal keys from environment")
        for v in os.environ:
            if v.startswith("VAULT_UNSEAL_KEY_"):
                keys.append(os.environ[v])
    if client.sys.is_sealed():
        print("unsealing vault using {}".format(keys))
        client.sys.submit_unseal_keys(keys)
        print(client.sys.is_sealed())


if __name__ == "__main__":
    main()
